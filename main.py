class Case:
    def __init__(self, player, nbSeed, id):
        self.player = player
        self.nbSeed = nbSeed
        self.id = id
        if id == 23: self.next = 0
        else: self.next = id + 1
        if id == 0: self.previous = 23
        else: self.previous = id - 1

    def __str__(self): return f"<{self.player} - {self.nbSeed} - {self.id}>" # - {self.next} - {self.previous}
    def __repr__(self): return self.__str__()

def initBoard():
    board = []
    boardTmp = []
    # Create board
    for i in range(0, int(sizeBoard / 2)):
        if i % 2 == 0:
            board.append(Case(1, nbSeedsOnPosition, i))
        else:
            board.append(Case(2, nbSeedsOnPosition, i))
    for i in range(int(sizeBoard / 2)):
        j = sizeBoard - i - 1
        if j % 2 == 0:
            boardTmp.append(Case(1, nbSeedsOnPosition, j))
        else:
            boardTmp.append(Case(2, nbSeedsOnPosition, j))
    # Arrange board
    boardTmp.reverse()
    return board + boardTmp

def printBoard():
    # Rearange board to print
    reverseBoard = board[12:]
    reverseBoard.reverse()
    print("\n\n")
    print(f"--- Board - Player {player} ---")
    print(reverseBoard)
    print(board[:12])
    print("--- Score ---")
    print(score)
    print("------")

def choosePlayer():
    print("Who start ? Press 1 or 2")
    choice = int(input())
    if choice == 1 or choice == 2: return choice
    else: choosePlayer()

def changePlayer(player):
    # Just change current player
    if player == 1: return 2
    else: return 1

def isVictory():
    flag = True
    # Test player victory
    if score[str(player)] > sizeBoard * nbSeedsOnPosition / 2:
        print(f"Player{player} Win !")
        flag = False
    # Test null
    if score["1"] == sizeBoard * nbSeedsOnPosition / 2 and score["2"] == sizeBoard * nbSeedsOnPosition / 2:
        print(f"Null !")
        flag = False
    return flag

def isHunger():
    seeds = 0
    # Test all positions
    for case in board:
        # Count in case of hunger
        seeds += case.nbSeed
        # If at least 1 position is valid, break
        if case.player == player and case.nbSeed > 0: return True
    # This is the case of player hunger, score seeds
    score[str(changePlayer(player))] += seeds
    # Print
    print(f"Player {player} is hunger, Player{changePlayer(player)} take {seeds}")
    # Break
    return False

def take(id):
    # Test if we have 2 or 3 seed to take
    if board[id].nbSeed == 2 or board[id].nbSeed == 3:
        # Add score for current player
        score[str(player)] += board[id].nbSeed
        # Recursive for previous positions
        take(board[id].previous)
        # Remove for taken positions
        board[id].nbSeed = 0

def ginner(id):
    if board[id].nbSeed != 0:
        # Initial next
        next = board[id].next
        # For every seeds
        for i in range(board[id].nbSeed):
            # Ginner current position
            board[next].nbSeed += 1
            # Save last position
            last = board[next].id
            # Save next position
            next = board[next].next
        # Reset starting position
        board[id].nbSeed = 0
        # Test take
        take(last)
    else: print("Empty case")

# --- Config ---
sizeBoard = 24
nbSeedsOnPosition = 4
board = initBoard()
score = {"1": 0, "2": 0}
# Choose player
player = choosePlayer()
flag = True
# --- Config ---

while flag == True:
    # Test if current player is hunger
    flag = isHunger()
    if flag == True:
        # Print the actual board
        printBoard()
        # Get action
        nb = int(input())
        if nb < 24 and nb >= 0 and board[nb].player == player and board[nb].nbSeed > 0:
            # Ginner seeds on selected case
            ginner(nb)
            # Print the board
            printBoard()
            # Test victory
            flag = isVictory()
            player = changePlayer(player)
        else: print("Bad player position")
printBoard()
print("- END -")
